using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Walid.Singleton
{
    public abstract class SingletonTemplate<T> : MonoBehaviour where T : Component
    {
        public static T Singleton
        {
            get;
            set;
        }

        public virtual void Awake()
        {
            if (Singleton is null)
                Singleton = this as T;
            else
                DestroyImmediate(gameObject);
        }
    }

    public abstract class DontDestroyableSingleton<T>: SingletonTemplate<T> where T: Component
    {
        public override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(Singleton.gameObject);
        }
    }
}
