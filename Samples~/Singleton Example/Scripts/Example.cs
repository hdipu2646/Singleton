using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Walid.Singleton;

//public class Example : SingletonTemplate<Example>
public class Example : DontDestroyableSingleton<Example>
{
    private void Start()
    {
        Debug.Log(Singleton.gameObject.name);
    }
}
